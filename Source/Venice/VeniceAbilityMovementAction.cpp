// Fill out your copyright notice in the Description page of Project Settings.


#include "VeniceAbilityMovementAction.h"

#include "Components/VeniceCharacterMovementComponent.h"

void UVeniceAbilityMovementAction::ToggleGroundFriction(bool EnableFriction)
{
	if (ActionOwner)
	{
		UVeniceCharacterMovementComponent *Movement = 
			Cast<UVeniceCharacterMovementComponent>(ActionOwner->GetCharacterMovement());

		if (Movement)
		{
			Movement->ToggleGroundFriction(EnableFriction);
		}
	}
}
