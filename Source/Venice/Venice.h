// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#define COLLISION_HIGHLIGHT			ECC_GameTraceChannel1
#define COLLISION_ATTACKS			ECC_GameTraceChannel2
