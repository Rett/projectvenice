// Fill out your copyright notice in the Description page of Project Settings.


#include "VeniceStatusEffectComponent.h"

#include "Net/UnrealNetwork.h"

#include "Venice/Components/VeniceHealthComponent.h"

// Sets default values for this component's properties
UVeniceStatusEffectComponent::UVeniceStatusEffectComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	MaxLifetime = 1.0;
	Damage = 10.0;
	EffectTickTime = 1.0;

	SetIsReplicated(true);
}

// Called when the game starts
void UVeniceStatusEffectComponent::BeginPlay()
{
	Super::BeginPlay();

	CurrentLifetime = MaxLifetime;

	GetWorld()->GetTimerManager().SetTimer(EffectTimerHandle, this, &UVeniceStatusEffectComponent::ServerEffectTick, EffectTickTime, true);
}

// Called every frame
void UVeniceStatusEffectComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UVeniceStatusEffectComponent::ServerEffectTick_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("TICK"));

	CurrentLifetime -= EffectTickTime;

	AActor *owner = GetOwner();
	if (!owner) return;

	EffectTick();

	UVeniceHealthComponent *health_comp = owner->FindComponentByClass<UVeniceHealthComponent>();

	if ((health_comp && health_comp->GetHealth() <= 0.0f) || CurrentLifetime <= 0.0f)
	{
		UE_LOG(LogTemp,Warning,TEXT("MyCharacter's Health is %f"), health_comp->GetHealth() );
		UE_LOG(LogTemp,Warning,TEXT("Lifetime %f"), CurrentLifetime );
		GetWorld()->GetTimerManager().ClearTimer(EffectTimerHandle);
		DestroyComponent(false);
		ConditionalBeginDestroy();
	}
}

void UVeniceStatusEffectComponent::OnRep_CurrentLifetime(float lifetime)
{
	if (!Instigator) return;
	OnStatusEffectTick.Broadcast(this, lifetime, Damage, nullptr, Instigator->GetInstigatorController(), GetOwner());
}

void UVeniceStatusEffectComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UVeniceStatusEffectComponent, CurrentLifetime);
}
