// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "VeniceCharacter.h"

#include "VeniceAbilityAction.generated.h"

UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UVeniceAbilityAction : public UObject
{
	GENERATED_BODY()
public:
	UVeniceAbilityAction();
	~UVeniceAbilityAction();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityComponent")
	TArray<UAnimMontage*> Animations;
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "AbilityAction", meta=(ToolTip="Run action (SERVER)"))
	void Run(AVeniceCharacter *Owner);
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "AbilityAction")
	void Tick(float DeltaTime);
	UFUNCTION(BlueprintCallable, Category = "AbilityAction")
	bool IsComplete();

protected:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "AbilityAction")
	void OnCompleted();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "AbilityAction")
	void Interrupt();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityAction")
	bool AffectFriendly;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AbilityAction")
	bool AffectEnemy;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityAction")
	float ActionTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityAction")
	AVeniceCharacter *ActionOwner;

	FTimerHandle ActionTimerHandle;
	bool Completed;
};
