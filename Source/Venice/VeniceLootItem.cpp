// Fill out your copyright notice in the Description page of Project Settings.

#include "VeniceLootItem.h"

#include "Net/UnrealNetwork.h"

// Sets default values
AVeniceLootItem::AVeniceLootItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AVeniceLootItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AVeniceLootItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AVeniceLootItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AVeniceLootItem, Item);
}
