// Fill out your copyright notice in the Description page of Project Settings.


#include "VeniceCharacterMovementComponent.h"

#include "GameFramework/Character.h"

UVeniceCharacterMovementComponent::UVeniceCharacterMovementComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UVeniceCharacterMovementComponent::SetMoveSpeed(float Speed)
{
	CurrentMoveSpeed = Speed;
	UseCustomMoveSpeed = true;
}

void UVeniceCharacterMovementComponent::ResetMoveSpeed()
{
	CurrentMoveSpeed = Super::GetMaxSpeed();

	UseCustomMoveSpeed = false;
}

void UVeniceCharacterMovementComponent::Dash(FVector Direction)
{
	//Send movement vector to server
	if (PawnOwner->GetLocalRole() < ROLE_Authority)
	{
		ServerSetDashDirection(Direction);
	}

	DashVector = Direction;
	UseDash = true;
}

float UVeniceCharacterMovementComponent::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();
	if (UseCustomMoveSpeed)
	{
		MaxSpeed = CurrentMoveSpeed;
	}

	return MaxSpeed;
}

float UVeniceCharacterMovementComponent::GetMaxAcceleration() const
{
	float MaxAccel = Super::GetMaxAcceleration();
	return MaxAccel;
}

void UVeniceCharacterMovementComponent::ToggleGroundFriction(bool EnableFriction)
{
	if (DefaultGroundFriction == 0.0f)
	{
		DefaultGroundFriction = GroundFriction;
	}
	if (DefaultWalkBrakingDeceleration == 0.0f)
	{
		DefaultWalkBrakingDeceleration = BrakingDecelerationWalking;
	}

	GroundFriction = EnableFriction ? DefaultGroundFriction : 0.0f;
	BrakingDecelerationWalking = EnableFriction ? DefaultWalkBrakingDeceleration : 0.0f;
}

//Set input flags on character from saved inputs
void UVeniceCharacterMovementComponent::UpdateFromCompressedFlags(uint8 Flags)//Client only
{
	Super::UpdateFromCompressedFlags(Flags);

	UseCustomMoveSpeed = (Flags&FSavedMove_Character::FLAG_Custom_0) != 0;
}

class FNetworkPredictionData_Client* UVeniceCharacterMovementComponent::GetPredictionData_Client() const
{
	check(PawnOwner != NULL);
	check(PawnOwner->GetLocalRole() < ROLE_Authority);

	if (!ClientPredictionData)
	{
		UVeniceCharacterMovementComponent* MutableThis = const_cast<UVeniceCharacterMovementComponent*>(this);

		MutableThis->ClientPredictionData = new FNetworkPredictionData_Client_MyMovement(*this);
		MutableThis->ClientPredictionData->MaxSmoothNetUpdateDist = 92.f;
		MutableThis->ClientPredictionData->NoSmoothNetUpdateDist = 140.f;
	}

	return ClientPredictionData;
}

bool UVeniceCharacterMovementComponent::ServerSetDashDirection_Validate(const FVector& DashDir)
{
	return true;
}

void UVeniceCharacterMovementComponent::ServerSetDashDirection_Implementation(const FVector& DashDir)
{
	DashVector = DashDir;
}

void UVeniceCharacterMovementComponent::OnMovementUpdated(float DeltaSeconds, const FVector& OldLocation, const FVector& OldVelocity)
{
	Super::OnMovementUpdated(DeltaSeconds, OldLocation, OldVelocity);

	if (!CharacterOwner)
	{
		return;
	}

	//Update dodge movement
	if (UseDash)
	{		
		FVector DodgeVel = DashVector;
		DodgeVel.Z = 0.0f;

		Launch(DodgeVel);

		UseDash = false;
	}
}

void FSavedMove_MyMovement::Clear()
{
	Super::Clear();

	SavedUseCustomMoveSpeed = false;
	SavedDashDirection = FVector::ZeroVector;
}

uint8 FSavedMove_MyMovement::GetCompressedFlags() const
{
	uint8 Result = Super::GetCompressedFlags();

	if (SavedUseCustomMoveSpeed)
	{
		Result |= FLAG_Custom_0;
	}

	return Result;
}

bool FSavedMove_MyMovement::CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const
{
	//This pretty much just tells the engine if it can optimize by combining saved moves. There doesn't appear to be
	//any problem with leaving it out, but it seems that it's good practice to implement this anyways.
	if (SavedUseCustomMoveSpeed != ((FSavedMove_MyMovement*)&NewMove)->SavedUseCustomMoveSpeed)
	{
		return false;
	}
	if (SavedDashDirection != ((FSavedMove_MyMovement*)&NewMove)->SavedDashDirection)
	{
		return false;
	}

	return Super::CanCombineWith(NewMove, Character, MaxDelta);
}

void FSavedMove_MyMovement::SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_Character & ClientData)
{
	Super::SetMoveFor(Character, InDeltaTime, NewAccel, ClientData);

	UVeniceCharacterMovementComponent* CharMov = Cast<UVeniceCharacterMovementComponent>(Character->GetCharacterMovement());
	if (CharMov)
	{
		SavedUseCustomMoveSpeed = CharMov->UseCustomMoveSpeed;

		//Again, just taking the player movement component's state and storing it for later it in the saved move.
		SavedDashDirection = CharMov->DashVector;
	}
}

void FSavedMove_MyMovement::PrepMoveFor(class ACharacter* Character)
{
	Super::PrepMoveFor(Character);

	UVeniceCharacterMovementComponent* CharMov = Cast<UVeniceCharacterMovementComponent>(Character->GetCharacterMovement());
	if (CharMov)
	{
		//This is just the exact opposite of SetMoveFor. It copies the state from the saved move to the movement
		//component before a correction is made to a client.
		CharMov->DashVector = SavedDashDirection;
		
		//Don't update flags here. They're automatically setup before corrections using the compressed flag methods.	
	}
}

FNetworkPredictionData_Client_MyMovement::FNetworkPredictionData_Client_MyMovement(const UCharacterMovementComponent& ClientMovement)
: Super(ClientMovement)
{

}

FSavedMovePtr FNetworkPredictionData_Client_MyMovement::AllocateNewMove()
{
	return FSavedMovePtr(new FSavedMove_MyMovement());
}
