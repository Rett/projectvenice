// Fill out your copyright notice in the Description page of Project Settings.


#include "VeniceAbilityMeleeAction.h"

#include "DrawDebugHelpers.h"
#include "Math/Quat.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"

#include "Venice.h"

void UVeniceAbilityMeleeAction::MultiSphereTraceStrike()
{
	TArray<FHitResult> HitResults;

	FVector ReachVector = ActionOwner->GetActorForwardVector() * Reach;

	FCollisionShape CollisionShape = FCollisionShape::MakeSphere(Radius);

	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(ActionOwner);
	QueryParams.bTraceComplex = true;
	StartLocation = ActionOwner->GetActorLocation();
	bool BlockingHit = ActionOwner->GetWorld()->SweepMultiByChannel(
		HitResults, StartLocation, StartLocation + ReachVector, FQuat(),
		COLLISION_ATTACKS, CollisionShape, QueryParams);
	FQuat DebugQuat(ActionOwner->GetActorRightVector(), 1.57f);
	FVector DebugReachVector = ActionOwner->GetActorForwardVector() * (Reach);
	
	if (BlockingHit)
	{
		TSubclassOf<UDamageType> DamageType;
		TArray<AActor*> HitActors;
		for (auto It = HitResults.CreateIterator(); It; It++)
        {
			FHitResult HitResult = (*It);
			if (HitResult.GetActor())
			{
				HitActors.AddUnique(HitResult.GetActor());
			}
        }

		DrawDebugCapsule(ActionOwner->GetWorld(), StartLocation + DebugReachVector, Reach, Radius, DebugQuat, FColor::Orange, false, 2.0f);
		for (AActor *Actor : HitActors)
		{
			UGameplayStatics::ApplyDamage(Actor, Damage, Actor->GetInstigatorController(), ActionOwner, DamageType);
		}
	}
	else
	{
		DrawDebugCapsule(ActionOwner->GetWorld(), StartLocation + DebugReachVector, Reach, Radius, DebugQuat, FColor::Purple, false, 2.0f);
	}
}
