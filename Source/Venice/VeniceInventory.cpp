// Fill out your copyright notice in the Description page of Project Settings.

#include "VeniceInventory.h"

UVeniceInventory::UVeniceInventory()
{
	Items.Push(0);
	Items.Push(1);
	Items.Push(2);
}

UVeniceInventory::~UVeniceInventory()
{
}

TArray<int> UVeniceInventory::GetItems() const
{
	return Items;
}

void UVeniceInventory::AddItem(int item)
{
	Items.Push(item);
}

void UVeniceInventory::RemoveItem(int item)
{
	Items.Remove(item);
}
