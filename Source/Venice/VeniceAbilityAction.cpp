// Fill out your copyright notice in the Description page of Project Settings.


#include "VeniceAbilityAction.h"

#include "VeniceCharacter.h"

UVeniceAbilityAction::UVeniceAbilityAction()
{
	AffectEnemy = true;
	AffectFriendly = false;
	ActionTime = 1.0;
}

UVeniceAbilityAction::~UVeniceAbilityAction()
{
}

void UVeniceAbilityAction::Run_Implementation(AVeniceCharacter *Owner)
{
	ActionOwner = Owner;
	Completed = false;
	ActionOwner->GetWorld()->GetTimerManager().SetTimer(ActionTimerHandle, this, &UVeniceAbilityAction::OnCompleted, ActionTime, false);
}

void UVeniceAbilityAction::OnCompleted_Implementation()
{
	Completed = true;
}

bool UVeniceAbilityAction::IsComplete()
{
	return Completed;
}

void UVeniceAbilityAction::Interrupt_Implementation()
{
	if (ActionOwner)
	{
		ActionOwner->GetWorld()->GetTimerManager().ClearTimer(ActionTimerHandle);
	}
	Completed = true;
}
