// Copyright Epic Games, Inc. All Rights Reserved.

#include "Venice.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Venice, "Venice" );
 