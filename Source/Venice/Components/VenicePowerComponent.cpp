// Fill out your copyright notice in the Description page of Project Settings.

#include "VenicePowerComponent.h"

#include "Net/UnrealNetwork.h"


// Sets default values for this component's properties
UVenicePowerComponent::UVenicePowerComponent()
{
	MaxPower = 100.0f;
	IsDead = false;

	SetIsReplicated(true);
}


// Called when the game starts
void UVenicePowerComponent::BeginPlay()
{
	Super::BeginPlay();

	Power = MaxPower;
}

void UVenicePowerComponent::OnRep_Power(float old_power)
{
	float power_delta = Power - old_power;
	OnPowerChanged.Broadcast(this, Power, power_delta);
}

float UVenicePowerComponent::GetPower() const
{
	return Power;
}

void UVenicePowerComponent::ServerUpdatePower_Implementation(float power_mod)
{
	if (IsDead)
	{
		return;
	}

	Power = FMath::Clamp(Power - power_mod, 0.0f, MaxPower);

	OnPowerChanged.Broadcast(this, Power, power_mod);
}


void UVenicePowerComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UVenicePowerComponent, Power);
}
