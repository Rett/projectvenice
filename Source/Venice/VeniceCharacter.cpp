// Copyright Epic Games, Inc. All Rights Reserved.

#include "VeniceCharacter.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "Components/VeniceAbilityComponent.h"
#include "Components/VeniceHealthComponent.h"
#include "Components/VenicePowerComponent.h"
#include "Components/VeniceCharacterMovementComponent.h"
#include "Components/VeniceAbilityComponent.h"
#include "Engine/LocalPlayer.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "InputActionValue.h"
#include "Net/UnrealNetwork.h"
#include "Venice.h"
#include "VeniceWeapon.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

//////////////////////////////////////////////////////////////////////////
// AVeniceCharacter

AVeniceCharacter::AVeniceCharacter(const FObjectInitializer& ObjectInitializer) : 
	ACharacter(ObjectInitializer.SetDefaultSubobjectClass<UVeniceCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
		
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	// bReplicates = true;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;
	GetCharacterMovement()->BrakingDecelerationFalling = 1500.0f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	HealthComp = CreateDefaultSubobject<UVeniceHealthComponent>(TEXT("HealthComp"));
	PowerComp = CreateDefaultSubobject<UVenicePowerComponent>(TEXT("PowerComp"));
	PrimaryAbility = CreateDefaultSubobject<UVeniceAbilityComponent>(TEXT("PrimaryAbility"));
	SecondaryAbility = CreateDefaultSubobject<UVeniceAbilityComponent>(TEXT("SecondaryAbility"));
	Ability1 = CreateDefaultSubobject<UVeniceAbilityComponent>(TEXT("Ability1"));
	Ability2 = CreateDefaultSubobject<UVeniceAbilityComponent>(TEXT("Ability2"));
	Ability3 = CreateDefaultSubobject<UVeniceAbilityComponent>(TEXT("Ability3"));
	Ability4 = CreateDefaultSubobject<UVeniceAbilityComponent>(TEXT("Ability4"));

	IsDead = false;
	AutoAttackStarted = false;
	CurrentWeapon = nullptr;
}

void AVeniceCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
	
	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
	
	HealthComp->OnHealthChanged.AddDynamic(this, &AVeniceCharacter::OnHealthChanged);
	Inventory = NewObject<UVeniceInventory>(this);
}

// Called every frame
void AVeniceCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector eye_location;
	FRotator eye_rotation;
	GetActorEyesViewPoint(eye_location, eye_rotation);

	FVector look_dir = eye_rotation.Vector();

	FCollisionQueryParams query_params;
	query_params.AddIgnoredActor(this);
	query_params.bReturnPhysicalMaterial = false;
	query_params.bTraceComplex = true;

	FVector trace_end = eye_location + (look_dir * 10000);
	FVector tracer_end_point = trace_end;

	EPhysicalSurface surface_type = SurfaceType_Default;
	FHitResult hit_result;
	bool blocking_hit = GetWorld()->LineTraceSingleByChannel(
		hit_result, eye_location, trace_end, COLLISION_HIGHLIGHT, query_params);

	if (blocking_hit)
	{
		Targeted(hit_result.GetActor());
	}
	else
	{
		Targeted(nullptr);
	}
}

AVeniceCharacterController* AVeniceCharacter::CharacterController() const
{
	return static_cast<AVeniceCharacterController*>( GetLocalViewingPlayerController() );
}

void AVeniceCharacter::Restart()
{
	Super::Restart();
}

// ONLY RAN ON SERVER
void AVeniceCharacter::AutoAttackHit(FAutoAttackInfo AttackInfo)
{
	if (GetLocalRole() < ROLE_Authority)
	{
		return;
	}

	AutoAttackStarted = false;

	AutoAttackNotify(AttackInfo);
}

float AVeniceCharacter::AutoAttackRange()
{
	return 250.0f;
}

void AVeniceCharacter::ServerAutoAttackHit_Implementation(FAutoAttackInfo AttackInfo)
{
	AutoAttackHit(AttackInfo);
}

bool AVeniceCharacter::ServerAutoAttackHit_Validate(FAutoAttackInfo AttackInfo)
{
	return true;
}


//////////////////////////////////////////////////////////////////////////
// Input

void AVeniceCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent)) {
		
		// Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		// Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AVeniceCharacter::Move);

		// Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &AVeniceCharacter::Look);

		// Abilities
		EnhancedInputComponent->BindAction(PrimaryAbilityInput, ETriggerEvent::Triggered,
			PrimaryAbility, &UVeniceAbilityComponent::CastAbility);
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

void AVeniceCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	
		// get right vector 
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement 
		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
	}
}

void AVeniceCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void AVeniceCharacter::ServerTarget_Implementation(AVeniceCharacter* target)
{
	Target = target;
}

bool AVeniceCharacter::ServerTarget_Validate(AVeniceCharacter* target)
{
	(void)target;
	return true;
}

void AVeniceCharacter::BeginAutoAttack()
{
	if (GetLocalRole() < ROLE_Authority)
	{
		ServerBeginAutoAttack();
	}

	AutoAttackStarted = true;
}

void AVeniceCharacter::ServerBeginAutoAttack_Implementation()
{
	BeginAutoAttack();
}

bool AVeniceCharacter::ServerBeginAutoAttack_Validate()
{
	return true;
}

void AVeniceCharacter::OnHealthChanged(UVeniceHealthComponent* Comp, float Health, 
	float HealthDelta, const class UDamageType* DamageType, 
	class AController* InstigatedBy, AActor* DamageCause)
{
	if (Health <= 0.0f && !IsDead)
	{
		IsDead = true;

		Die();

		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		DetachFromControllerPendingDestroy();
		SetLifeSpan(10.0f);
	}
}

FVector AVeniceCharacter::GetPawnViewLocation() const
{
	if (CameraComp)
	{
		return CameraComp->GetComponentLocation();
	}

	return Super::GetPawnViewLocation();
}

void AVeniceCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AVeniceCharacter, IsDead);
	DOREPLIFETIME(AVeniceCharacter, AutoAttackStarted);
	DOREPLIFETIME(AVeniceCharacter, Target);
	DOREPLIFETIME(AVeniceCharacter, Inventory);
}

