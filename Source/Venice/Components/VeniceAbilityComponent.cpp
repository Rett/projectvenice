// Fill out your copyright notice in the Description page of Project Settings.


#include "VeniceAbilityComponent.h"

#include "Net/UnrealNetwork.h"

#include "Venice/VeniceCharacter.h"

// Sets default values for this component's properties
UVeniceAbilityComponent::UVeniceAbilityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicated(true);
}


// Called when the game starts
void UVeniceAbilityComponent::BeginPlay()
{
	Super::BeginPlay();
	CooldownRemaining = 0.0f;
	ActionIndex = 0;
	Casting = false;
}


bool UVeniceAbilityComponent::CanCastAbility()
{
	return (CooldownRemaining <= 0.0f);
}

void UVeniceAbilityComponent::OnRep_CooldownRemaining(float old_cooldown)
{
	OnCooldownUpdated.Broadcast(this, Cooldown, CooldownRemaining, Slot, nullptr);
}

void UVeniceAbilityComponent::CastAbility()
{
	if (!CanCastAbility() || Actions.Num() <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("CANT CAST ABILITY %d %d"), (int)CanCastAbility(), ((int)Actions.Num() <= 0));
		return;
	}

	ActionIndex = 0;
	UVeniceAbilityAction *action = Actions[ActionIndex]->GetDefaultObject<UVeniceAbilityAction>();
	AVeniceCharacter *character = Cast<AVeniceCharacter>(GetOwner());
	if (character)
	{
		Casting = true;

		ServerCastAbility();
	}
}

void UVeniceAbilityComponent::CooldownFinished_Implementation()
{
}

void UVeniceAbilityComponent::ServerCastAbility_Implementation()
{
	if (!CanCastAbility() || Actions.Num() <= 0)
	{
		return;
	}

	ActionIndex = 0;
	UVeniceAbilityAction *action = Actions[ActionIndex]->GetDefaultObject<UVeniceAbilityAction>();
	AVeniceCharacter *character = Cast<AVeniceCharacter>(GetOwner());
	if (character)
	{
		action->Run(character);
		OnAbilityActionBegin.Broadcast(action);
		GetWorld()->GetTimerManager().SetTimer(CooldownTimerHandle, this, &UVeniceAbilityComponent::CooldownFinished, Cooldown, false);
		Casting = true;
	}
}

// Called every frame
void UVeniceAbilityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetOwnerRole() == ROLE_Authority)
	{
		float PrevCooldownRemaining = CooldownRemaining;
		CooldownRemaining = GetWorld()->GetTimerManager().GetTimerRemaining(CooldownTimerHandle);
		if (CooldownRemaining != PrevCooldownRemaining)
		{
			OnCooldownUpdated.Broadcast(this, Cooldown, CooldownRemaining, Slot, GetOwner()->GetInstigatorController());
		}
	}

	if (Casting)
	{
		UVeniceAbilityAction *ActiveAction = Actions[ActionIndex]->GetDefaultObject<UVeniceAbilityAction>();
		ActiveAction->Tick(DeltaTime);

		if (ActiveAction->IsComplete())
		{
			ActionIndex++;
			if (ActionIndex >= Actions.Num())
			{
				Casting = false;
				ActionIndex = 0;
			}
			else
			{
				UVeniceAbilityAction *NextAction = Actions[ActionIndex]->GetDefaultObject<UVeniceAbilityAction>();
				AVeniceCharacter *character = Cast<AVeniceCharacter>(GetOwner());
				if (character)
				{
					if (GetOwnerRole() == ROLE_Authority)
					{
						NextAction->Run(character);
						OnAbilityActionBegin.Broadcast(NextAction);
					}
				}
			}
		}
	}
}

void UVeniceAbilityComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UVeniceAbilityComponent, CooldownRemaining);
}

