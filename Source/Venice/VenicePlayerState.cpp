// Fill out your copyright notice in the Description page of Project Settings.


#include "VenicePlayerState.h"

#include "Net/UnrealNetwork.h"

void AVenicePlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AVenicePlayerState, PlayerName);
}
