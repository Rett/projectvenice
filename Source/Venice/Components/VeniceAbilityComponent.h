// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Venice/VeniceAbilityAction.h"
#include "VeniceAbilityComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FiveParams(FOnCooldownUpdatedSignature, UVeniceAbilityComponent*, AbilityComp, float, Cooldown, float, CooldownRemaining, int, Slot, class AController*, InstigatedBy );
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAbilityActionBegin, UVeniceAbilityAction*, Action);


UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UVeniceAbilityComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UVeniceAbilityComponent();
	
	UFUNCTION(BlueprintCallable, Category = "AbilityComponent")
	void CastAbility();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AbilityComponent")
	int Slot;
	UPROPERTY(ReplicatedUsing = OnRep_CooldownRemaining, BlueprintReadOnly, Category = "AbilityComponent")
	float CooldownRemaining;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AbilityComponent")
	float Cooldown;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityComponent")
	TArray<TSubclassOf<UVeniceAbilityAction>> Actions;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AbilityComponent")
	bool Casting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityComponent")
	UTexture2D* Icon;
	
	UFUNCTION(BlueprintCallable, Category = "AbilityComponent")
	bool CanCastAbility();
	UFUNCTION()
	void OnRep_CooldownRemaining(float old_cooldown);
	UFUNCTION(BlueprintNativeEvent, Category = "AbilityComponent")
	void CooldownFinished();
	UFUNCTION(Server, Reliable)
	void ServerCastAbility();

	int ActionIndex;
	FTimerHandle CooldownTimerHandle;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnCooldownUpdatedSignature OnCooldownUpdated;
	
	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnAbilityActionBegin OnAbilityActionBegin;
};
