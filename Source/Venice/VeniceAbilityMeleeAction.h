// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VeniceAbilityAction.h"
#include "VeniceAbilityMeleeAction.generated.h"

/**
 * 
 */
UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UVeniceAbilityMeleeAction : public UVeniceAbilityAction
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable, Category = "AbilityMeleeAction")
	void MultiSphereTraceStrike();
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityMeleeAction")
	FVector StartLocation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityMeleeAction")
	float Reach;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityMeleeAction")
	float Radius;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityMeleeAction")
	float AttackDelay;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityMeleeAction")
	float Damage;
};
