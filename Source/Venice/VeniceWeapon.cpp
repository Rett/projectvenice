// Fill out your copyright notice in the Description page of Project Settings.

#include "VeniceWeapon.h"

// Sets default values
AVeniceWeapon::AVeniceWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SkeletalMeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshComp"));
	SkeletalMeshComp->SetupAttachment(RootComponent);
	SkeletalMeshComp->SetIsReplicated(true);

	BaseDamage = 25.0f;
	CriticalRate = 0.01f;
	CriticalMultiplier = 1.5f;
	PrimarySocketName = "Socket";
	Reach = 250.0f;
}

// Called when the game starts or when spawned
void AVeniceWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AVeniceWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FVector AVeniceWeapon::PrimarySocketLocation() const
{
	return SkeletalMeshComp->GetSocketLocation(PrimarySocketName);
}

float AVeniceWeapon::GetReach() const
{
	return Reach;
}

