// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "VenicePlayerState.generated.h"

/**
 * 
 */
UCLASS()
class VENICE_API AVenicePlayerState : public APlayerState
{
	GENERATED_BODY()

protected:
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "User")
	FString PlayerName;
};
