// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Blueprint/UserWidget.h"

#include "VeniceCharacterController.generated.h"

class AVeniceCharacter;
/**
 * 
 */
UCLASS()
class VENICE_API AVeniceCharacterController : public APlayerController
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = UserInterface, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class UUserWidget> HudType;
	
public:
	AVeniceCharacterController();
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void UpdateTarget(AVeniceCharacter *Target);

protected:
	// To add mapping context
	virtual void BeginPlay() override;
	
	UPROPERTY(BlueprintReadOnly)
	UUserWidget* Hud;
};
