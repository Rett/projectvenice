// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VenicePowerComponent.generated.h"

// OnPowerChanged Event
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnPowerChangedSignature, UVenicePowerComponent*, PowerComp, float, Power, float, PowerDelta);

UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UVenicePowerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UVenicePowerComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(ReplicatedUsing = OnRep_Power, BlueprintReadOnly, Category = "PowerComponent")
	float Power;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PowerComponent")
	float MaxPower;

	UFUNCTION()
	void OnRep_Power(float old_power);
	
	UFUNCTION(Server, Reliable, BlueprintAuthorityOnly, BlueprintCallable)
	void ServerUpdatePower(float power_mod);

	bool IsDead;

public:	
	float GetPower() const;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnPowerChangedSignature OnPowerChanged;
};
