// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VeniceGameMode.generated.h"

UCLASS(minimalapi)
class AVeniceGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AVeniceGameMode();
//
// protected:
// 	virtual void HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer) override;	
};



