// Copyright Epic Games, Inc. All Rights Reserved.

#include "VeniceGameMode.h"

#include "GameFramework/PlayerStart.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "UObject/ConstructorHelpers.h"
#include "VeniceCharacter.h"
#include "VeniceCharacterController.h"

AVeniceGameMode::AVeniceGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ArenaCharacter"));
	static ConstructorHelpers::FClassFinder<AVeniceCharacterController> PlayerControllerBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonController"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}

// void AVeniceGameMode::HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer)
// {
// 	Super::HandleStartingNewPlayer_Implementation(NewPlayer);
//  
// 	AVeniceCharacter* TempCharToCheck = Cast<AVeniceCharacter>(NewPlayer->GetPawn());
// 	if(TempCharToCheck)
// 	{
// 		return;
// 	}
// 	
// 	if(NewPlayer->GetPawn())
// 	{
// 		NewPlayer->GetPawn()->Destroy();
// 	}
//  
// 	FTransform tSpawnTransform;
// 	TArray<AActor*> foundEntries;
// 	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AVeniceCharacter::StaticClass(), foundEntries);
//  
// 	if(foundEntries.Num() > 0)
// 	{
// 		tSpawnTransform = foundEntries[0]->GetActorTransform();
// 	}
// 	else
// 	{
// 		TArray<AActor*> foundPlayerStarts;
// 		UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), foundPlayerStarts);
// 		tSpawnTransform = foundPlayerStarts[0]->GetActorTransform();
// 	}
//  
// 	float fTempFloatX = UKismetMathLibrary::RandomBool() ? UKismetMathLibrary::RandomFloatInRange(-50.f, -150.0f) : UKismetMathLibrary::RandomFloatInRange(50.f, 150.0f);
// 	float fTempFloatY = UKismetMathLibrary::RandomFloatInRange(-50.0f, -150.0f);
//  
// 	tSpawnTransform.SetLocation((tSpawnTransform.GetLocation() + FVector(fTempFloatX, fTempFloatY, 0.0f)));
// 	
//     FActorSpawnParameters SpawnInfo;
// 	SpawnInfo.Owner = NewPlayer;
// 	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
//  
// 	FVector TempLoc = tSpawnTransform.GetLocation();
// 	FRotator TempRot = tSpawnTransform.GetRotation().Rotator();
//     AVeniceCharacter* TempChar = Cast<AVeniceCharacter>(GetWorld()->SpawnActor(AVeniceCharacter::StaticClass(), &TempLoc, &TempRot, SpawnInfo));
// 	NewPlayer->Possess(TempChar);
// }