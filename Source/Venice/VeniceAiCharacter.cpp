// Copyright Epic Games, Inc. All Rights Reserved.

#include "VeniceAiCharacter.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "Components/VeniceAbilityComponent.h"
#include "Components/VeniceHealthComponent.h"
#include "Components/VenicePowerComponent.h"
#include "Components/VeniceCharacterMovementComponent.h"
#include "Components/VeniceAbilityComponent.h"
#include "Engine/LocalPlayer.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "InputActionValue.h"
#include "Net/UnrealNetwork.h"
#include "Venice.h"
#include "VeniceWeapon.h"

//////////////////////////////////////////////////////////////////////////
// AVeniceAiCharacter

AVeniceAiCharacter::AVeniceAiCharacter(const FObjectInitializer& ObjectInitializer) : 
	ACharacter(ObjectInitializer.SetDefaultSubobjectClass<UVeniceCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
		
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	// bReplicates = true;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;
	GetCharacterMovement()->BrakingDecelerationFalling = 1500.0f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	HealthComp = CreateDefaultSubobject<UVeniceHealthComponent>(TEXT("HealthComp"));
	PowerComp = CreateDefaultSubobject<UVenicePowerComponent>(TEXT("PowerComp"));
	PrimaryAbility = CreateDefaultSubobject<UVeniceAbilityComponent>(TEXT("PrimaryAbility"));
	SecondaryAbility = CreateDefaultSubobject<UVeniceAbilityComponent>(TEXT("SecondaryAbility"));
	Ability1 = CreateDefaultSubobject<UVeniceAbilityComponent>(TEXT("Ability1"));
	Ability2 = CreateDefaultSubobject<UVeniceAbilityComponent>(TEXT("Ability2"));
	Ability3 = CreateDefaultSubobject<UVeniceAbilityComponent>(TEXT("Ability3"));
	Ability4 = CreateDefaultSubobject<UVeniceAbilityComponent>(TEXT("Ability4"));

	IsDead = false;
}

void AVeniceAiCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
	
	HealthComp->OnHealthChanged.AddDynamic(this, &AVeniceAiCharacter::OnHealthChanged);
}

// Called every frame
void AVeniceAiCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AVeniceAiCharacter::Restart()
{
	Super::Restart();
}

void AVeniceAiCharacter::OnHealthChanged(UVeniceHealthComponent* Comp, float Health, 
	float HealthDelta, const class UDamageType* DamageType, 
	class AController* InstigatedBy, AActor* DamageCause)
{
	if (Health <= 0.0f && !IsDead)
	{
		IsDead = true;

		Die();

		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		DetachFromControllerPendingDestroy();
		SetLifeSpan(10.0f);
	}
}

FVector AVeniceAiCharacter::GetPawnViewLocation() const
{
	if (CameraComp)
	{
		return CameraComp->GetComponentLocation();
	}

	return Super::GetPawnViewLocation();
}

void AVeniceAiCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AVeniceAiCharacter, IsDead);
	DOREPLIFETIME(AVeniceAiCharacter, Target);
}

