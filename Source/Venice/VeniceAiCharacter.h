// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Logging/LogMacros.h"
#include "UObject/UnrealTypePrivate.h"
#include "VeniceAiCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;

class USphereComponent;
class UVeniceHealthComponent;
class UVenicePowerComponent;
class UVeniceAbilityComponent;

UCLASS(config=Game)
class AVeniceAiCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;
	
public:
	AVeniceAiCharacter(const FObjectInitializer& ObjectInitializer);
	
protected:
	UFUNCTION(BlueprintImplementableEvent, Category = "Character")
	void Targeted(AActor* target);
	UFUNCTION(BlueprintImplementableEvent, Category = "Character")
	void Die();
	UFUNCTION()
	void OnHealthChanged(UVeniceHealthComponent* Comp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCause);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UCameraComponent* CameraComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USpringArmComponent* SpringArmComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UVeniceHealthComponent* HealthComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UVenicePowerComponent* PowerComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UVeniceAbilityComponent* PrimaryAbility;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UVeniceAbilityComponent* SecondaryAbility;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UVeniceAbilityComponent* Ability1;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UVeniceAbilityComponent* Ability2;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UVeniceAbilityComponent* Ability3;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UVeniceAbilityComponent* Ability4;
	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Character")
	bool IsDead;
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Character")
	AActor* Target;
	
	// To add mapping context
	virtual void BeginPlay();
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	virtual void Restart() override;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
	
	FVector GetPawnViewLocation() const override;
};

