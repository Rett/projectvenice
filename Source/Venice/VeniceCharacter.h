// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VeniceCharacterController.h"
#include "GameFramework/Character.h"
#include "Logging/LogMacros.h"
#include "UObject/UnrealTypePrivate.h"
#include "VeniceInventory.h"
#include "VeniceCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UInputMappingContext;
class UInputAction;
struct FInputActionValue;

DECLARE_LOG_CATEGORY_EXTERN(LogTemplateCharacter, Log, All);

class USphereComponent;
class UVeniceHealthComponent;
class UVenicePowerComponent;
class UVeniceAbilityComponent;
class AVeniceWeapon;

UENUM(BlueprintType)
enum class EAttackSide : uint8
{
	LEFT,
	RIGHT
};

USTRUCT(BlueprintType)
struct FAutoAttackInfo
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, Category="Auto Attack Info")
	EAttackSide AutoAttackSide;
};

UCLASS(config=Game)
class AVeniceCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;
	
	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* JumpAction;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveAction;

	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* LookAction;
	
	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* PrimaryAbilityInput;

	UFUNCTION(BlueprintCallable, Category = "Character")
	void AutoAttackHit(FAutoAttackInfo AttackInfo);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerAutoAttackHit(FAutoAttackInfo AttackInfo);
	UFUNCTION(BlueprintCallable, Category = "Character")
	void BeginAutoAttack();

	virtual float AutoAttackRange();
	
public:
	AVeniceCharacter(const FObjectInitializer& ObjectInitializer);
	
	UFUNCTION(BlueprintCallable)
	AVeniceCharacterController* CharacterController() const;
	
protected:
	/** Called for movement input */
	void Move(const FInputActionValue& Value);

	/** Called for looking input */
	void Look(const FInputActionValue& Value);
	
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerTarget(AVeniceCharacter* target);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerBeginAutoAttack();

	UFUNCTION(BlueprintImplementableEvent, Category = "Character")
	void AutoAttack();
	UFUNCTION(BlueprintImplementableEvent, Category = "Character")
	void Targeted(AActor* target);
	UFUNCTION(BlueprintImplementableEvent, Category = "Character")
	void AutoAttackNotify(FAutoAttackInfo AttackInfo);
	UFUNCTION(BlueprintImplementableEvent, Category = "Character")
	void Die();
	UFUNCTION()
	void OnHealthChanged(UVeniceHealthComponent* Comp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCause);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UCameraComponent* CameraComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USpringArmComponent* SpringArmComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UVeniceHealthComponent* HealthComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UVenicePowerComponent* PowerComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UVeniceAbilityComponent* PrimaryAbility;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UVeniceAbilityComponent* SecondaryAbility;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UVeniceAbilityComponent* Ability1;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UVeniceAbilityComponent* Ability2;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UVeniceAbilityComponent* Ability3;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UVeniceAbilityComponent* Ability4;
	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Character")
	bool IsDead;
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Character")
	bool AutoAttackStarted;
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Character")
	AActor* Target;
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Character")
	AVeniceWeapon* CurrentWeapon;
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Character")
	UVeniceInventory* Inventory;

	
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	// To add mapping context
	virtual void BeginPlay();
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	virtual void Restart() override;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
	
	FVector GetPawnViewLocation() const override;
};

